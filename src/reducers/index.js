import { combineReducers } from 'redux';
import EventReducer from './EventReducer';
import SelectionReducer from './SelectionReducer';



import type {EventState} from "./EventReducer";
import type {SelctionState} from "./SelectionReducer";

export type AppState = {
    event_state:EventState,
    selection_state: SelctionState
}

export const ReducersNamespace = {
    event_state:"EventState",
    selection_state:"SelectionEstate",
};

// It is better practice to name the reducers
// https://redux.js.org/recipes/structuring-reducers/using-combinereducers
const reducers = combineReducers({
    event_state:EventReducer,
    selection_state:SelectionReducer,
});


export default reducers;

