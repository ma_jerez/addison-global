import SelectionReducer, {initVal} from './SelectionReducer';
import {ActionID} from "../actions/SelectionActions";
import {getMockSelection,getMockUserSelected} from '../api/mockData';
import type {AppAction} from "../types/types";


const selection = getMockSelection();
const user_selected = getMockUserSelected();

it('Selection Reducer should return the initial state', () => {
    expect(SelectionReducer(undefined, {})).toEqual(initVal);
});

it(`Selection Reducer should handle  ${ActionID.ADD_SELECTION}`, () => {
    //here we mock the action
    const action: AppAction = {
        type: ActionID.ADD_SELECTION,
        text: selection.name,
        payload: selection
    };

    //the reduced value should contain now the selection passed by the action
    const reduced = {
        user_selected:user_selected,
        open_drawer:true
    };
    expect(SelectionReducer(initVal, action)).toEqual(reduced);

});

it(`Selection Reducer should handle  ${ActionID.REMOVE_SELECTION}`, async () => {

    //here we mock the action
    const action: AppAction = {
        type: ActionID.REMOVE_SELECTION,
        text: selection.name,
        payload: selection
    };

    //intial state contains the user selected
    const state = {
        user_selected:user_selected,
        open_drawer:true
    };

    //the item must has been removed from the user_selected object
    const reduced = {
        user_selected:{},
        open_drawer:true
    };
    expect(SelectionReducer(state, action)).toEqual(reduced);

});

