import {ActionID} from "../actions/SelectionActions";
import type {SelectionType, SelectionMap, AppAction} from "../types/types";


export type SelctionState = {
    user_selected: SelectionMap,
    open_drawer:boolean
}


export const initVal = {
    user_selected: {},
    open_drawer:false
};

// we initialize the stae object
// we always returns ope_drawer == true after add or remove an item so the drawers is always visible showing the actions
const SelectionReducer = (state = initVal, action: AppAction<SelectionType>) => {
    let selID = null;
    switch (action.type) {
        case ActionID.ADD_SELECTION:
            selID = action.payload.id;
            state.user_selected[selID] = action.payload;
            const aux = {
                user_selected:{
                    ...state.user_selected
                },
                open_drawer:true
            };
            return aux;


        case ActionID.REMOVE_SELECTION:
            selID = action.payload.id;
            if (typeof state.user_selected[selID] !== 'undefined')
                delete state.user_selected[selID];
            return {
                user_selected:{
                    ...state.user_selected
                },
                open_drawer:true
            };

        default:
            return state
    }
}


export default SelectionReducer;