import EventReducer , {initVal} from './EventReducer';
import {ActionID} from "../actions/EventApiActions";
import {getEvents} from '../api/mockData';
import type {AppAction} from "../types/types";




it('Event Reducer should return the initial state' , ()=>{
    expect(EventReducer(undefined, {})).toEqual(initVal);
});
it(`Event Reducer should handle ${ActionID.GET_EVENTS}`, async ()=>{
    const action:AppAction = {
        type:ActionID.GET_EVENTS
    };
    expect(EventReducer(initVal, action)).toEqual(initVal);

});
it(`Event Reducer should handle ${ActionID.GET_EVENTS_ERROR}`, async ()=>{

    //here we mock the action
    const action:AppAction = {
        type:ActionID.GET_EVENTS_ERROR
    };
    const expected = {
        entities:{},
        loading:false,
        error:true
    };
    expect(EventReducer(initVal, action)).toEqual(expected);

});

it(`Event Reducer should handle ${ActionID.GET_EVENTS_SUCCESS}`, async ()=>{

    //here we mock the action values
    const data = await getEvents();
    const action:AppAction = {
        type:ActionID.GET_EVENTS_SUCCESS,
        payload: data
    };

    const expected ={
        entities:data.entities,
        loading:false,
        error:false
    };
    expect(EventReducer(initVal, action)).toEqual(expected);
});

