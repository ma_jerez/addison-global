import {ActionID} from "../actions/EventApiActions";
import type {APIresponse} from "../types/types";
import type {AppAction} from "../types/types";
import type {Entities} from "../types/types";

export type EventState = {
    loading:boolean,
    error:Error,
    entities:Entities,
}


export const initVal = {
    loading:true,
    error:null,
    entities:null,
};


// this can be be called before there is any state so store = {} set and mepty state by default
const EventReducer = ( state = initVal, action:AppAction<APIresponse>)=>{

    switch (action.type) {
        //requesting event data to the api
        case ActionID.GET_EVENTS:
            return {
                ...state
            };

        //data from the api correctly fetched
        case ActionID.GET_EVENTS_SUCCESS:
            return {
                entities:action.payload.entities,
                loading:false,
                error:false
            };

        //error from the API
        case ActionID.GET_EVENTS_ERROR:
            return {
                entities:{},
                loading:false,
                error:true
            };
        default:
            return state
    }
}


export default EventReducer;