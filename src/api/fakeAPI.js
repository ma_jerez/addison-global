import {normalize} from "normalizr";
import eventList from "./schema";
import type {SelectionType, UserType, APIresponse} from "../types/types";

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


const $fakenetworkDelay = 1000;

//this simulates get events from the API
export const getEvents = async ():APIresponse => {
    const response:APIresponse = await fetch('http://www.mocky.io/v2/59f08692310000b4130e9f71');
    const json = await response.json();
    const norm =  normalize(json,eventList);
    await  timeout($fakenetworkDelay);
    return {
        success:true,
        status:'selection for user x saved',
        entities: norm.entities
    }
};


//####################
// Bellow Methods are for testing purposes only
//####################




export const getEventsFails = async ():APIresponse => {
    await  timeout($fakenetworkDelay);
    return {
        success:false,
        status:'error retrieving the data',
    }
}


//this simulates a call to the API the slection ont he server
export const saveSeletion = async (userId:UserType,selection:SelectionType)=>{
     await  timeout($fakenetworkDelay);
     return {
         success:true,
         status:'selection for user x saved'
     }
};

export const saveSeletionFails = async (userId:UserType,selection:SelectionType)=>{
    await  timeout($fakenetworkDelay);
    return {
        success:false,
        status:'fail saving selection for user x, try later'
    }
};





