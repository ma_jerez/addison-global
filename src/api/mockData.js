import {getEvents as getEventsFromAPI} from "./fakeAPI";
import {getParentEvent, getParentMarket} from "./entities";


let mockData = null;

/**
 * Returns and objec wiht all the data required for testing
 * @returns {Promise<{entities: *, selection: {id: string, name: string, price: number, parent: string, user_selected: boolean}, user_selected: {SEL_1: {id: string, name: string, price: number, parent: string, user_selected: boolean}}, market: *, event: *}>}
 */
export const getMockData = async()=>{
    if(!mockData){
        const data = await getEventsFromAPI();

        const market = getParentMarket(data.entities,getMockSelection());
        const event = getParentEvent(data.entities,market);
        mockData = {
            entities:data.entities,
            selection: getMockSelection(),
            user_selected:getMockUserSelected(),
            market:market,
            event:event
        }
    }
    return mockData;
};


/**
 * Returns a Selection to use in mock test
 * @returns {{id: string, name: string, price: number, parent: string, user_selected: boolean}}
 */
export const getMockSelection = ()=>{
    return {"id": "SEL_1", "name": "Real Madrid", "price": 1.23, "parent": "MKT_1", "user_selected": true};
};


/**
 * Returns and initialised user_select object to use in testing.
 * user_select is the object that stores the user's selected "Selections"
 * @returns {{SEL_1: {id: string, name: string, price: number, parent: string, user_selected: boolean}}}
 */
export const getMockUserSelected = ()=>{
    return {
        'SEL_1':getMockSelection()
    }
};


export const getEvents =  getEventsFromAPI;


