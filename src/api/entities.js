import type {
    EventType,
    MarketType,
    SelectionType,
    Entities, SelectionMap,
} from "../types/types";




export function getEvents(ent: Entities): EventType[] {
    return Object.keys(ent.events).map(key => ent.events[key]);
}

export function getChildMarkets(ent: Entities, event: EventType): [MarketType] {
    return Object.keys(ent.markets).map(key => ent.markets[key]).filter(m => m.parent === event.id);
}

export function getChildSelections(ent: Entities, market: MarketType): [MarketType] {
    return Object.keys(ent.selections).map(key => ent.selections[key]).filter(s => s.parent === market.id);
}


export function getParentEvent(ent: Entities, market: MarketType){
    return ent.events[market.parent];
}


export function getParentMarket(ent: Entities, selection: SelectionType){
    return ent.markets[selection.parent];
}


/**
 * Find  All the Events  that contains a selection from the user_selection.
 * this is a reverse lookup from bottom to top.
 * @param ent
 * @param user_selection
 * @returns {EventType[]}
 */
export function getUserSelectedEvents(ent: Entities,user_selection:SelectionMap):EventType[]{
    const markets =  getUserSelectedMarkets(ent,user_selection);
    return Object.keys(ent.events).map(key => ent.events[key]).filter(event =>{
        let found = false;
        markets.forEach((market)=>{
            found = found || (market.parent === event.id)
        });
        return found;
    } );
}


/**
 * Finds all teh Markets that contains a selection from the user_selection.
 * this is a reverse lookup from bottom to top.
 * @param ent
 * @param user_selection
 * @returns {MarketType[]}
 */
export function getUserSelectedMarkets(ent:Entities,user_selection:SelectionMap):MarketType[]{
    return Object.keys(ent.markets).map(marketKey => ent.markets[marketKey]).filter(market =>{
        let found = false;
        Object.keys(user_selection).forEach((sel_key)=>{
            const selection = ent.selections[sel_key];
            found = found || (selection.parent === market.id)
        });
        return found;
    } );
}

/**
 * Finds all the Marker that contains a selection from the user_selection if the Markets belong
 * to the passed Event.
 * @param ent
 * @param user_selection
 * @param event
 */
export function getUserSelectedMarketsByEvent(ent:Entities,user_selection:SelectionMap,event:EventType){
    return  getUserSelectedMarkets(ent,user_selection).filter(market=>market.parent === event.id);
}



export function getSelectBymarket(user_selection:SelectionMap,market:MarketType){
    return Object.keys(user_selection).map(k => user_selection[k]).filter(selection=>selection.parent === market.id)
}
