import {schema} from 'normalizr';


function processStrategy(value, parent, key) {
    return {...value, parent: parent.id};
}


const Selection = new schema.Entity(
    'selections',
    {},
    {
        processStrategy: (value, parent, key) => {
            return {...value, parent: parent.id, user_selected: false};
        }
    }
);

const Market = new schema.Entity(
    'markets',
    {selections: [Selection]},
    {processStrategy}
);

const Event = new schema.Entity(
    'events',
    {markets: [Market]},
);


const eventList = [Event];
export default eventList;
