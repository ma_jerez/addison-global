import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {getEventsFromAPI, ActionID} from './EventApiActions';
import {getEvents} from '../api/mockData';


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


it(`Should create the actions ${ActionID.GET_EVENTS} and ${ActionID.GET_EVENTS_SUCCESS} after successfully 
get the events from the API`, async () => {
    const events = await getEvents();
    const expectedActions = [
            {
                type: ActionID.GET_EVENTS,
                text: 'getting events from the API'
            },
            {
                type: ActionID.GET_EVENTS_SUCCESS,
                payload: events
            }
    ];

    const store = mockStore({});
    return store.dispatch(getEventsFromAPI()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions);
    });
});