import {ActionID,addSelection,removeSelection} from './SelectionActions';
import {getMockSelection} from '../api/mockData';


const selection = getMockSelection();


it(`Should create an action to ${ActionID.ADD_SELECTION}`, () => {
    const expected = {
        type: ActionID.ADD_SELECTION,
        text: selection.name,
        payload: selection
    };
    expect(addSelection(selection)).toEqual(expected);
});



it(`Should create an action to ${ActionID.REMOVE_SELECTION}`, () => {
    const expected = {
        type: ActionID.REMOVE_SELECTION,
        text: selection.name,
        payload: selection
    };
    expect(removeSelection(selection)).toEqual(expected);
});