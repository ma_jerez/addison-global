import {getEvents} from '../api/fakeAPI';
import type {APIresponse} from "../types/types";

export const ActionID ={
    'GET_EVENTS':'GET_EVENTS',
    'GET_EVENTS_SUCCESS' :'GET_EVENTS_SUCCESS',
    'GET_EVENTS_ERROR':'GET_EVENTS_ERROR',
};



export const getEventsFromAPI = ()=>{
    return async (dispatch) => {

        /** dispatch the loading from api event action **/
        dispatch({ type: ActionID.GET_EVENTS,text:'getting events from the API'});


        /** Dispatch success action **/
        function onSuccess(data:APIresponse) {
            //dispatch th suscces event
            dispatch({ type: ActionID.GET_EVENTS_SUCCESS, payload: data });
            return data;
        }

        /** Dispatch error action **/
        function onError(error:Error) {
            dispatch({ type: ActionID.GET_EVENTS_ERROR, error });
            return error;
        }


        try {
            const resp:APIresponse = await getEvents();
            return onSuccess(resp);
        } catch (error) {
            return onError(error);
        }
    }
};


