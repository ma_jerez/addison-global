import type {AppAction} from "../types/types";


export const ActionID ={
    'ADD_SELECTION':'ADD_SELECTION',
    'REMOVE_SELECTION':'REMOVE_SELECTION'
};



export const addSelection = (selection:SelectionType): AppAction => {
    return {
        type: ActionID.ADD_SELECTION,
        text: selection.name,
        payload: selection
    };
};

export const  removeSelection = (selection:SelectionType): AppAction => {
    return {
        type: ActionID.REMOVE_SELECTION,
        text: selection.name,
        payload: selection
    };
};