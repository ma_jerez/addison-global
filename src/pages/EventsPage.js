import React, {Component} from "react";
import {Drawer, NavBar} from 'antd-mobile';
import {MdMenu, MdClose} from 'react-icons/lib/md';
import EventList from '../components/EventList';
import type {Entities, EventType, SelectionMap} from "../types/types";
import {getEvents,getUserSelectedEvents} from "../api/entities";
import Loading from '../components/Loading';
import styles from "./EventsPage.less";


type props = {
    user_selected:SelectionMap|boolean,
    entities: Entities,
    loading:boolean,
    events:EventType[],
    open:boolean,
    // closeDrawer:()=>void,
    // toogleDrawer:()=>void,
    // openDrawer:()=>void
}

export default class EventsPage extends Component {

    props:props;

    static defaultProps = {
        user_selected:false,
        entities:null,
        events:[],
        loading:true,
        open:false
    };

    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }


    /**
     * Updates the drawe open state whr the prop open is modified by the reducers.
     * @param newProps
     */
    componentWillReceiveProps= (newProps)=>{
        if(typeof newProps.open !== 'undefined' && newProps.open){
            this.openDrawer();
        }
    };

    sideBar = ()=>{
        return (
            <div className='events-sidebar'>
                <NavBar rightContent={<MdClose size={30} onClick={this.closeDrawer} className={styles.clickIcon}/>}>&nbsp;</NavBar>
                <EventList
                    entities={this.props.entities}
                    user_selected={this.props.user_selected}
                    events={getUserSelectedEvents(this.props.entities,this.props.user_selected)}/>
            </div>
        )
    };

    render() {

        if (this.props.entities && !this.props.loading) {
            return (
                <div className="EventsPage">
                    <Drawer
                        className={styles.SelectionsDrawer}
                        style={{minHeight: document.documentElement.clientHeight}}
                        contentStyle={{color: '#A6A6A6', textAlign: 'center'}}
                        sidebar={this.sideBar()}
                        open={this.state.open}
                        onOpenChange={this.onOpenChange}
                        position='right'
                    >
                        <NavBar
                            rightContent={<MdMenu size={30} onClick={this.onOpenChange} className="clickIcon"/>}>Addison
                            Global</NavBar>
                        <EventList
                            entities={this.props.entities}
                            user_selected={false}
                            events={getEvents(this.props.entities)}/>
                    </Drawer>
                </div>
            );
        } else {
            return (
                <Loading/>
            );
        }

    }

    closeDrawer = () => {
        this.setState({open: false});
    }

    openDrawer = () => {
        this.setState({open: true});
    }
    onOpenChange = () => {
        console.log(this.state);
        this.setState({open: !this.state.open});
    }


}