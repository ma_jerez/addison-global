// @flow

import React, {Component} from 'react';
import {EventsPage} from "../containers/EventsPage";
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import { getEventsFromAPI } from "../actions/EventApiActions";
import styles from "./App.less"


const middleware = [ thunk ];
//we add redux-lgger only for deelopment and not running a test
if (process.env.NODE_ENV !== 'production' && typeof it === 'undefined') {
    console.log(navigator.userAgent);
    middleware.push(createLogger());
}

const store = createStore(
    reducers,
    applyMiddleware(...middleware)
);

store.dispatch(getEventsFromAPI());


export default class App extends Component {
    /**
     * here we will use  a router instead render the page directly
     * @returns {*}
     */
    render() {
        return (
            <Provider store={store}>
                <div className={styles.App}>
                    <EventsPage/>
                </div>
            </Provider>
        );
    }
}

