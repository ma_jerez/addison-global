// @flow

// ################
//     Actions
// ################


export type AppAction<T> = {
    type: string,
    text?: string,
    payload?:T,
}


// ################
//     Models
// ################


/** Response from the API */
export type APIresponse = {
    success:boolean,
    status:'text',
    entities?:Entities
}


/** Event Entity */
export type EventType = {
    id: string,
    name: string,
};

/** Market Entity */
export type MarketType = {
    id: string,
    name: string,
    parent: string // the id of the parent Event
}

/** Selection Entity */
export type SelectionType = {
    id: string,
    name: string,
    price: number,
    user_selected?:boolean,
    parent:string // the id of the parent Market
}

/** User Entity */
export type UserType = {
    id: string,
    name: string
}


/** The response type of the API call */
export type EventState = EventType[];


// ################
//     Normalized data
// ################

/** A map object of  Selections */
export type SelectionMap = { [id: string]: SelectionType }

/** A map object of  Markets */
export type MarketMap = { [id: string]: MarketType }


/** A map object of  Events */
export type EventMap = { [id: string]: EventType }


/** normalized Data */
export type Entities = {
    selections : SelectionMap,
    markets: MarketMap,
    events: EventMap
}





