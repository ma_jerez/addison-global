import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import type {AppState} from "../reducers/index";
import SelectionListView from '../components/SelectionList';
import {addSelection,removeSelection} from "../actions/SelectionActions";


const mapStateToProps = ({selection_state}: AppState) => {
    return {
        user_selected: selection_state.user_selected
    }
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            addSelection: addSelection,
            removeSelection: removeSelection
        },
        dispatch
    )
}


export const SelectionList = connect(mapStateToProps, mapDispatchToProps)(SelectionListView);