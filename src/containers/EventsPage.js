import {connect} from 'react-redux'


import type {AppState} from "../reducers/index";
import EventsPageView from "../pages/EventsPage";




const mapStateToProps = ({event_state,selection_state}:AppState) => {
    return {
        loading:event_state.loading,
        entities: event_state.entities,
        user_selected:selection_state.user_selected,
        open:selection_state.open_drawer
    }
};


export const EventsPage = connect(mapStateToProps)(EventsPageView);

