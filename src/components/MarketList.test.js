import React from 'react';
import {shallow} from 'enzyme';
import {getMockData} from "../api/mockData";
import MarketList from './MarketList';
import {getChildMarkets, getChildSelections} from "../api/entities";





it('Renders a list of markets', async () => {
    const data = await getMockData();
    const ent = data.entities;
    const markets  = getChildMarkets(data.entities,data.event);
    const List = shallow(
        <MarketList
            user_selected={data.user_selected}
            markets={markets}
            entities={ent}/>
    );
    const marketWrapper = List.find("Market");
    expect(List).toHaveLength(1);
    expect(marketWrapper.length).toBeGreaterThanOrEqual(1);

});
