import { Card, WingBlank, WhiteSpace } from 'antd-mobile';
import React, {Component} from "react";
import MarketList from './MarketList';
import type {Entities, EventType, MarketType, SelectionMap} from '../types/types';
import styles from "./Event.less";


type props = {
    entities:Entities,
    event:EventType,
    markets:MarketType[],
    user_selected: SelectionMap | boolean,
};

export default class Event extends Component {
    props:props;


    render() {

        if(this.props.markets.length > 0){
            return (
                <WingBlank size="lg">
                    <WhiteSpace size="lg" />
                    <Card>
                        <Card.Header
                            title={<h3 className={styles.eventTitle}>{this.props.event.name}</h3>}
                        />
                        <Card.Body>
                            <MarketList
                                user_selected={this.props.user_selected}
                                markets={this.props.markets}
                                entities={this.props.entities}/>
                        </Card.Body>
                    </Card>
                    <WhiteSpace size="lg" />
                </WingBlank>
            );
        }
        return null;
    }
}



