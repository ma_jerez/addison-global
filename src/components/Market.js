import React, {Component} from 'react';
import type {Entities, SelectionMap, SelectionType} from '../types/types';
import {getChildSelections,getSelectBymarket} from "../api/entities";
import {SelectionList} from '../containers/SelectionList';
import styles from './Market.less';

type props = {
    entities:Entities,
    market: SelectionType,
    user_selected: SelectionMap | boolean,
};

/**
 * Market
 */
export default class Market extends Component {
    props: props;


    isSelected = ()=>{
        return (this.props.user_selected && Object.keys(this.props.user_selected).length);
    };

    getSelections = ()=>{
        if(this.isSelected()){
            return getSelectBymarket(this.props.user_selected,this.props.market);
        }else{
            return getChildSelections(this.props.entities,this.props.market);
        }
    };

    render() {
        return (
            <div className={styles.market}>
                <p className={styles.marketTitle}>{this.props.market.name}</p>
                <SelectionList
                    entities={this.props.entities}
                    user_selected={this.props.user_selected}
                    isSelected={this.isSelected()}
                    selections={this.getSelections()}/>
            </div>
        );
    }
}
