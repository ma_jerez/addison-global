import React, {Component} from "react";
import {Button} from 'antd-mobile';
import type {SelectionType} from '../types/types';
import styles from './Selection.less';


type props = {
    selection: SelectionType,
    currentSelected: SelectionType,
    onClick: () => void
}

export default class Selection extends Component {
    props: props;



    render() {
        return (
            <div className="space-between sec">
                <Button inline
                        type={(this.props.selected)? "primary": "default"}
                        size='small'
                        className={styles.selbtn}
                        onClick={() => {
                            this.props.onClick(this)
                        }}>
                    {this.props.selection.name}
                    <br/>
                    {this.props.selection.price}
                </Button>
            </div>
        )
    }
}