import React from 'react';
import {shallow} from 'enzyme';
import {getMockData} from "../api/mockData";
import EventList from './EventList';
import {getChildMarkets, getChildSelections, getEvents} from "../api/entities";





it('Renders a list of events with selection items', async () => {
    const data = await getMockData();
    const ent = data.entities;
    const events  = getEvents(ent);
    const List = shallow(
        <EventList
            entities={data.entities}
            user_selected={false}
            events={events}/>
    );
    const eventWrapper = List.find("Event");
    expect(List).toHaveLength(1);
    expect(eventWrapper.length).toBeGreaterThanOrEqual(1);
});
