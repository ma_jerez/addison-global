import React from 'react';
import {shallow} from 'enzyme';
import {getMockData} from "../api/mockData";
import Market from './Market'




it('Renders Selection without break', async () => {
    const data = await getMockData();
    const market = data.market;
    const marketWrapper = shallow(
        <Market
            market={market}
            entities={data.entities}
            user_selected={data.user_selected}
        />
    );
    expect(marketWrapper).toHaveLength(1);
});

