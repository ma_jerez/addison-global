import React, {Component} from "react";
import type {EventType, Entities, SelectionMap} from '../types/types';
import Event from './Event';
import {getChildMarkets,getUserSelectedMarketsByEvent} from "../api/entities";



type props = {
    entities: Entities,
    events: [EventType],
    user_selected: SelectionMap | boolean,
}

export default class EventList extends Component {
    props: props;

    static defaultProps = {
        user_selected: false,
        entities: {},
        events: []
    };


    getMarkets = (event)=>{
        if(this.props.user_selected && Object.keys(this.props.user_selected)){
            return getUserSelectedMarketsByEvent(this.props.entities,this.props.user_selected,event);
        }else{
            return getChildMarkets(this.props.entities,event);
        }
    }

    render() {

        const events = this.props.events.map((event:EventType) => {
                return (
                    <Event
                        event={event}
                        key={event.id}
                        entities={this.props.entities}
                        user_selected={this.props.user_selected}
                        markets={this.getMarkets(event)}/>
                )
            }
        );


        return (
            <div className="event-list">
                {events}
            </div>
        );
    }
}
