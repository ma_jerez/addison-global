import React from 'react';
import {shallow} from 'enzyme';
import {getMockData} from "../api/mockData";
import SelectionList from './SelectionList';
import {getChildSelections} from "../api/entities";





it('Renders the normal Selected List when user_selected is undefined and isSelected is false', async () => {
    const data = await getMockData();
    const ent = data.entities;
    const selections  = getChildSelections(ent,data.market);
    const List = shallow(
        <SelectionList
            entities={ent}
            isSelected={false}
            selections={selections}/>
    );
    const selectedWrapper = List.find("Selection");
    expect(List).toHaveLength(1);
    expect(selectedWrapper.length).toBeGreaterThanOrEqual(1);

});


it('Renders the user selected list when user_selected is passed and isSelected is true', async () => {
    const data = await getMockData();
    const ent = data.entities;
    const selections  = getChildSelections(ent,data.market);
    const user_selected = data.user_selected;
    const List = shallow(
        <SelectionList
            entities={ent}
            isSelected={true}
            user_selected={user_selected}
            selections={selections}/>
    );
    const selectedWrapper = List.find("UserSelected");
    expect(List).toHaveLength(1);
    expect(selectedWrapper.length).toBeGreaterThanOrEqual(1);
});