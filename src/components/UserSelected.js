import React, {Component} from "react";
import {Button} from 'antd-mobile';
import type {SelectionType} from '../types/types';

type props = {
    selection: SelectionType,
    currentSelected: SelectionType,
    onClick: () => void
}

export default class UserSelected extends Component {
    props: props;

    render() {
        return (
            <div className="space-between already-sec">
                {this.props.selection.name} {this.props.selection.price}
                <br/>
                <Button inline
                        type="warning"
                        size='small'
                        className='selection-btn-small'
                        onClick={() => {
                            this.props.onClick(this)
                        }}>
                    Remove
                </Button>
            </div>
        )
    }
}