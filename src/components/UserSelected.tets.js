import React from 'react';
import {shallow} from 'enzyme';
import UserSelected from './UserSelected';
import {getMockSelection} from "../api/mockData";

const selection = getMockSelection();
const userSelBtn = shallow(
    <UserSelected
        selection={selection}
        onClick={()=>true}
        selected={true}
    />
);


it('Renders User Selected button without break', () => {
    expect(userSelBtn).toHaveLength(1);
});


it('Renders User Selected button and button and type == warning', () => {
    const button = unsellectedButton.find('Button');
    expect(button.props().type).toBe("warning");
});


