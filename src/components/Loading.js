import React, {Component} from "react";
import {NavBar, ActivityIndicator} from 'antd-mobile';
import styles from './Loading.less';

export default class Event extends Component {

    render() {

        return(
            <div className={styles.loading}>
                <NavBar className={styles.clickIcon}>Addison Global</NavBar>
                <div className={styles.loadingContent}>
                    <ActivityIndicator
                        toast
                        text="Loading..."
                    />
                </div>
            </div>
        )
    }
}