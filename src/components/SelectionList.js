import React, {Component} from "react";
import type {SelectionMap, SelectionType} from '../types/types';
import Selection from './Selection';
import UserSelected from './UserSelected'
import {getChildSelections, getParentMarket} from "../api/entities";
import styles from './SelectionList.less';

type props = {
    selections: SelectionType[],
    user_selected: SelectionMap | boolean,
    addSelection: (selection: SelectionType) => void,
    removeSelection: (selection: SelectionType) => void,
    isSelected:boolean
}




export default class SelectionList extends Component {
    props: props;


    constructor(props) {
        super(props);
        this.state = {
            currentSelected: null
        }
    }



    getSelectedSiblin = (siblings:SelectionType[])=>{
        const userSel = (this.props.user_selected && Object.keys(this.props.user_selected).length);
        if(!userSel)
            return false;
        let currentSeleted = false;
        siblings.forEach(sibling=>{
            if(currentSeleted)
                return;
            else if(typeof this.props.user_selected[sibling.id] !== 'undefined'){
                currentSeleted = this.props.user_selected[sibling.id];
            }
        });
        return currentSeleted;
    };

    multyToogle = (selComponet: Selection) => {
        const selection = selComponet.props.selection;
        if(this.isSelectionSelected(selection)){
            // we remove the current selections
            this.props.removeSelection(selection);
        }else{
            const market = getParentMarket(this.props.entities,selection);
            const siblings = getChildSelections(this.props.entities,market);
            const currentSeleted =  this.getSelectedSiblin(siblings);

            if(currentSeleted){
                //do nothing we need to deselect frist
                // we remove the current selections
                this.props.removeSelection(currentSeleted);
                this.props.addSelection(selection);
            }else{
                // we do nothing no tposiible to selct another until we deselect
                this.props.addSelection(selection);
            }
        }
    };


    isSelectionSelected = (selection:SelectionType)=>{
        const userSel = (this.props.user_selected && Object.keys(this.props.user_selected).length);
        const match = (userSel)?(this.props.user_selected[selection.id]):false;
        const selected = (userSel && typeof match !== 'undefined');
        return selected;
    };


    deleteSelection = (selComponet: Selection) => {
        // we remove the current selections
        const selection = selComponet.props.selection;
        this.props.removeSelection(selection);
    };

    render() {
        const selections = this.props.selections.map(
            (selection: SelectionType) => {
                if(this.props.isSelected){
                    return (
                        <UserSelected
                            selection={selection}
                            key={selection.id}
                            onClick={this.deleteSelection.bind(this)}
                        />
                    )
                }else{
                    return (
                        <Selection
                            selection={selection}
                            key={selection.id}
                            onClick={this.multyToogle.bind(this)}
                            selected={this.isSelectionSelected(selection)}
                        />
                    )
                }
            }
        );


        return (
            <div className={styles.selectionList}>
                {selections}
            </div>
        );
    }
}

