import React from 'react';
import {shallow} from 'enzyme';
import {getMockData} from "../api/mockData";
import Event from './Event'
import {getChildMarkets} from "../api/entities";




it('Renders An Event without break', async () => {
    const data = await getMockData();
    const event = data.event;
    const markets =  getChildMarkets(data.entities,event);
    const wrapper = shallow(
        <Event
            event={event}
            entities={data.entities}
            user_selected={data.user_selected}
            markets={markets}/>
    );
    expect(wrapper).toHaveLength(1);
});