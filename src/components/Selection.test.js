import React from 'react';
import {shallow} from 'enzyme';
import Selection from './Selection';
import {getMockSelection} from "../api/mockData";

const selection = getMockSelection();
const selectedButton = shallow(
    <Selection
        selection={selection}
        onClick={()=>true}
        selected={true}
    />
);

const unsellectedButton = shallow(
    <Selection
        selection={selection}
        onClick={()=>true}
        selected={false}
    />
);

it('Renders Selection without break', () => {
    expect(selectedButton).toHaveLength(1);
});


it('Renders Selection and button is not selected (type == default)', () => {
    const button = unsellectedButton.find('Button');
    expect(button.props().type).toBe("default");
});


it('Renders Selection and button is  selected (type == primary)', () => {
    const button = selectedButton.find('Button');
    expect(button.props().type).toBe("primary");
});

