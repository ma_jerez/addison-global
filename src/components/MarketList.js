import React, {Component} from "react";
import Market from './Market';
import type {Entities, MarketType, SelectionMap} from '../types/types';

type props = {
    markets: MarketType[],
    entities:Entities,
    user_selected: SelectionMap | boolean,
}

export default class MarketList extends Component {
    props: props;


    render() {

        const markets = this.props.markets.map(
            (market: MarketType) => {
                return (
                    <Market
                        key={market.id}
                        market={market}
                        entities={this.props.entities}
                        user_selected={this.props.user_selected}
                    />
                )
            }
        );

        return (
            <div className="market-list">
                {markets}
            </div>
        );
    }
}
