#Addison Global React Test 

This project was bootstrapped using [Create React App](https://github.com/facebookincubator/create-react-app).

The main goal of this project is demonstrate proficiency at javascript, react and critical thinking.  
Despite not being too familiar whit React I've chosen to solve this exercise using it, 
react is a must-to nowadays.


### installing dependencies 
1. install [yarn](https://yarnpkg.com/en/docs/install) package manager.
2. install project packages  `yarn install`


### Running the app

* `npm start` to start a developemnt server
* `npm test` to run all the tests
* `npm run build` to build a production app



###  Technology Stack
Choosing the right stack technology is one of the most important decision for the viability and future of a project.  
Due to the characteristics of this app (Sport Bets) we could choose to develop a multi-platform 
app using a mobile hybrid development approach as the app would not require access to any of the phone's natives APIs 
(camera, gps etc...)   
Another approach would be to use native development using rect-native + react-native-web, take into account that 
using this approach will require two maintain a native + web version for any custom component.

As per this exercise we going to implement only a WEB APP using the bellow technology stack:

* React + Redux (No router required for this exercise as there is only one view)
* Css modules: we are using [css modules](https://github.com/css-modules/css-modules) to automatically scope the css rules.
* Ant Design mobile as UI framework + less  (We could use any other UI framework)
* Jest + Enzyme for testing



### Best practices
* **Smoke Testing:** minimal testing for each component that ensures components doesn't break the build. 
this test are simple to implement and adds high value to code quality.
* **Unit testing:** unit testing must be balanced, using too much unit testing [might be useless](https://medium.com/@drozzy/why-unit-testing-sucks-eb87176f310b)
and reduce the development speed.   
Therefore a good balance must be keep between well tested component and room for code refactoring without breaking everything. 
* **Integration test:** will use Enzyme for integration test.
* **End to End testing:** E2E testing usually requires a lot of developer resources. 
[cypress.io](https://www.cypress.io/) is a new modern tool  that greatly simplifies E2E testing.
* **Typed Javascript**: Using a typed system detect a big quantity of errors at compile time. 
typed system reduce boilerplate code on unit testing to just test properties adn simplifies refactoring.  
* **We are going to use [Flow](https://flow.org/) as it is already supported by create-react-app.** 
Using flow also makes redundant the user of [prop-types](https://www.npmjs.com/package/prop-types) in components, a common pattern
in react.  
Prop-types checks types at runtime so still relevant to detect incorrect types from user input 
(although user input should have validation), or inconsistent data fetching.   
See this [stackOverflow](https://stackoverflow.com/questions/36065185/react-proptypes-vs-flow?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa) 
thread for more info.
* **Eslint:** for code consistency and static error detection (complements types javascript). 

*Due to time constrains for this project, not all this best practices might be implemented.


### App State management
**State Managment:** Redux is used to manage the state of the App. the goal is to remove any state management 
and business logic form the views and keep it in a central store.   
Views react to changes in the state of this central store.

### Data Normalization
Relational data represented as Nested structures and  might be a problem when working with reducers.
* Nested data complicates the reducer logic.   
* **When working with immutable data, the full source tree is copied causing unnecessary re-rendering of 
all the componet linked to the tree.**  

Source: 'normalizing state shape' [Redux docs](https://redux.js.org/recipes/structuring-reducers/normalizing-state-shape) 

We are using the [normalizr](https://github.com/paularmstrong/normalizr) library to manage the normalization of the data. 
   
* We will keep a collection of 3 different Entities `Event` `market` ans `Selection`. 
* Entities will be accessed by ID.   
* Each `Selection` will keep a reference it's parent `Market`.  
* Each `Market` will keep a reference to it's parent `Event`.
* Normalization is done when data is requested from the API.
* Once we get the data we will store the entities in the state and share across All components. 
  
An example of the normalized data structure can be found in the file `src/api/normalized-output.json`.

 


### Other tools
* [CodeClimate](https://codeclimate.com/) is a code quality tool, it can be integrated wiht github and bitbucket 
(this is a paid product so its not used in this project)
* [Greenkeeper](https://github.com/apps/greenkeeper) is a bot to keep npm dependencies updated. 
(this is paid for private repos, so not used in this project)
* [Cicle ci](https://circleci.com/) is a continuous integration tool, can also be used for Continuous Delivery
  see this [article](https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd)  for mor info.




### tests
To tun the test watchman must be installed on the system `brew install watchman`

To run the test execute the command `npm run test`. There are unit test for actions, reducers and react components.

There is an utility module in `src/api/mocData.js`, use this module to get the mock data when writing the tests.







     


